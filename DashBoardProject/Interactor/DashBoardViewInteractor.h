//
//  DashBoardViewInteractor.h
//  DashBoardProject
//
//  Created by AMK on 28/03/18.
//  Copyright © 2018 SchoolSpeak. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DashBoardMemberAssociationsVM.h"

@interface DashBoardViewInteractor : UIView

- (DashBoardMemberAssociationsVM *)currentMemberAssociations;

- (NSArray *)resourcesDisplayListForCurrentSelectedMember;

- (void)updateMemberAssociation:(DashboardMemberVM *)member;

@end
