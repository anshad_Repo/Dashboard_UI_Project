//
//  DashBoardViewInteractor.m
//  DashBoardProject
//
//  Created by AMK on 28/03/18.
//  Copyright © 2018 SchoolSpeak. All rights reserved.
//

#import "DashBoardViewInteractor.h"
#import "DashBoardResourceCollectionItem.h"

@interface DashBoardViewInteractor()

@property (nonatomic, strong) DashBoardMemberAssociationsVM *currentSelectedMemberAssociations;
@property (nonatomic, strong) NSMutableArray <DashBoardResourceCollectionItem*> *resourceDisplayListForCurrentMember;

@end

@implementation DashBoardViewInteractor

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

#pragma mark - Getters

- (DashBoardMemberAssociationsVM *)currentSelectedMemberAssociations{
    
    if(!_currentSelectedMemberAssociations){
        
        _currentSelectedMemberAssociations = [DashBoardMemberAssociationsVM new];
        
        //Dummy data entries.
        DashboardMemberVM *craig = [DashboardMemberVM new];
        craig.imageName = @"craig";
        craig.displayName = @"Daniel Craig";
        
        DashboardMemberVM *gates = [DashboardMemberVM new];
        gates.displayName = @"Bill Gates";
        gates.imageName = @"gates";
        
        DashboardMemberVM *leanardo = [DashboardMemberVM new];
        leanardo.displayName = @"Leanardo de Caprio";
        leanardo.imageName = @"leanardo";
        
        DashboardMemberVM *jobs = [DashboardMemberVM new];
        jobs.displayName = @"Steve Jobs";
        jobs.imageName = @"steve";
        
        DashboardMemberVM *brad = [DashboardMemberVM new];
        brad.displayName = @"Brad Pit";
        brad.imageName = @"brad";
        
        self.currentSelectedMemberAssociations.associatedMembers = [[NSMutableArray alloc] initWithObjects:gates, nil];
                                                                    
        self.currentSelectedMemberAssociations.currentSelectedMember = craig;
        
    }
    return _currentSelectedMemberAssociations;
}


- (NSMutableArray *)resourceDisplayListForCurrentMember{
    
    if(!_resourceDisplayListForCurrentMember){
        _resourceDisplayListForCurrentMember = [NSMutableArray new];
        
        //Dummy data entries;
        for(int i = 0;i< 12;i++){
            
            DashBoardResourceCollectionItem *item = [DashBoardResourceCollectionItem new];
            item.resourceImage = @"test";
            item.resourceDisplayName = @"Calander";
            if(i%2 == 0) item.notificationsCount = @"13";
            [self.resourceDisplayListForCurrentMember addObject:item];
        }
    }
    return _resourceDisplayListForCurrentMember;
}

#pragma mark -

- (NSArray *)resourcesDisplayListForCurrentSelectedMember{
    
    return self.resourceDisplayListForCurrentMember;
}

- (DashBoardMemberAssociationsVM *)currentMemberAssociations{
    
    return self.currentSelectedMemberAssociations;
}


- (void)updateMemberAssociation:(DashboardMemberVM *)selectedMember{
    
    DashboardMemberVM *previousSelectedMember = self.currentSelectedMemberAssociations.currentSelectedMember;
    self.currentSelectedMemberAssociations.currentSelectedMember = selectedMember;
    
    NSMutableArray *membersArray = [self.currentSelectedMemberAssociations.associatedMembers mutableCopy];
    for(DashboardMemberVM *member in self.currentSelectedMemberAssociations.associatedMembers){
        
        if([member.displayName isEqualToString:selectedMember.displayName]){
            
            NSInteger index = [self.currentSelectedMemberAssociations.associatedMembers indexOfObject:member];
            
            [membersArray replaceObjectAtIndex:index withObject:previousSelectedMember];
        }
    }
    
    self.currentSelectedMemberAssociations.associatedMembers = membersArray.copy;
    [self updateResourceReslourceDisplayListForSelectedMember:selectedMember];
}

- (void)updateResourceReslourceDisplayListForSelectedMember:(DashboardMemberVM *)member{
    
}


@end
