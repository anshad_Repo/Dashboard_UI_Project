//
//  UserProfileCollectionViewLayout.m
//  DashBoardProject
//
//  Created by AMK on 27/03/18.
//  Copyright © 2018 SchoolSpeak. All rights reserved.
//
#import "UserProfileCollectionViewLayout.h"

@interface UserProfileCollectionViewLayout()

@property (nonatomic, strong) NSMutableArray *sectionCellSizes;
@property (nonatomic, strong) NSMutableArray *cellLayoutAttributes;
@property (nonatomic, assign) CGSize contentSize;
@property (nonatomic, assign) BOOL didPrepareLayout;

@end

@implementation UserProfileCollectionViewLayout


- (void)prepareLayout{
    
    if (self.didPrepareLayout) {
        
        return;
    }
    
    self.cellLayoutAttributes   = [NSMutableArray new];
    self.sectionCellSizes   = [NSMutableArray new];
    
    NSInteger numberOfSections = [self.collectionView numberOfSections];

    if (self.sectionCellSizes.count != numberOfSections) {
        [self calculateSectionItemSize];
    }
    
    CGFloat xOffset = 0.0;
    CGFloat yOffset = 0.0;
    CGSize itemSize = CGSizeZero;
    NSInteger cellSerialIndex = 0;
    
    for (int section = 0; section < numberOfSections; section++) {
        
        NSMutableArray *rowAttributes = [@[] mutableCopy];
        
        
        NSUInteger numberOfItems = [self.collectionView numberOfItemsInSection:section];
        for (NSUInteger row = 0; row < numberOfItems; row++) {
            
            itemSize = [[self.sectionCellSizes objectAtIndex:cellSerialIndex] CGSizeValue];;
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:row inSection:section];
            UICollectionViewLayoutAttributes *attributes = [UICollectionViewLayoutAttributes layoutAttributesForCellWithIndexPath:indexPath];
            attributes.frame = CGRectIntegral(CGRectMake(xOffset, yOffset, itemSize.width, itemSize.height));
            
            [rowAttributes addObject:attributes];
            
            yOffset +=itemSize.height + 1.0;
            
            cellSerialIndex++;
        }
        
        [self.cellLayoutAttributes addObject:rowAttributes];
        
        xOffset += itemSize.width + 4.0;
        yOffset = 0;
        
    }
    
    CGFloat collectionViewContentWidth = xOffset;
    CGFloat collectionViewContentHeight = yOffset;
    
    self.contentSize = CGSizeMake(collectionViewContentWidth, collectionViewContentHeight);
    
    self.didPrepareLayout = YES;
}

- (CGSize)collectionViewContentSize{
    return self.contentSize;
}

- (UICollectionViewLayoutAttributes *)layoutAttributesForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    return self.cellLayoutAttributes[indexPath.section][indexPath.row];
}


- (NSArray *)layoutAttributesForElementsInRect:(CGRect)rect{
    
    NSMutableArray *attributes = [@[] mutableCopy];
    
    NSPredicate *predicate = [NSPredicate predicateWithBlock:^BOOL(UICollectionViewLayoutAttributes *evaluatedObject, NSDictionary *bindings) {
        return CGRectIntersectsRect(rect, [evaluatedObject frame]);
    }];

    
    for (NSArray *section in self.cellLayoutAttributes) {
        [attributes addObjectsFromArray:[section filteredArrayUsingPredicate:predicate]];
    }
    
    return attributes;
}

- (void)calculateSectionItemSize{
    
    NSUInteger numberOfSections = [self.collectionView numberOfSections];
    for (NSUInteger section = 0; section < numberOfSections; section++) {
        
        NSInteger numberOfRowsInSection = [self.collectionView numberOfItemsInSection:section];
        for(NSInteger row = 0; row < numberOfRowsInSection; row++){
        id<UICollectionViewDelegateFlowLayout>delegate = (id<UICollectionViewDelegateFlowLayout>)self.collectionView.delegate;
        
            CGSize cellSize = CGSizeZero;
            if ([delegate respondsToSelector:@selector(collectionView:layout:sizeForItemAtIndexPath:)]) {
                
                cellSize = [delegate collectionView:self.collectionView layout:self
                            sizeForItemAtIndexPath:[NSIndexPath indexPathForRow:row inSection:section]];
                            
                NSValue *cellSizeSizeValue = [NSValue valueWithCGSize:cellSize];
                [self.sectionCellSizes addObject:cellSizeSizeValue];
            }
        }
    }
}

@end
