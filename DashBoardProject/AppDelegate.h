//
//  AppDelegate.h
//  DashBoardProject
//
//  Created by Anshad M K on 22/03/18.
//  Copyright © 2018 Anshad M K. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

