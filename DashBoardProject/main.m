//
//  main.m
//  DashBoardProject
//
//  Created by Anshad M K on 22/03/18.
//  Copyright © 2018 Anshad M K. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
