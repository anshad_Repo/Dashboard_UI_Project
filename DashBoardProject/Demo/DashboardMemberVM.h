//
//  UserViewModel.h
//  DashBoardProject
//
//  Created by AMK on 28/03/18.
//  Copyright © 2018 SchoolSpeak. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DashboardMemberVM : NSObject

@property (nonatomic, strong) NSString *displayName;
@property (nonatomic, strong) NSString *imageName;

@end
