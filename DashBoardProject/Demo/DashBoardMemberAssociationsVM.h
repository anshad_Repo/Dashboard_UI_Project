//
//  DashBoardMemberAssociationsVM.h
//  DashBoardProject
//
//  Created by AMK on 28/03/18.
//  Copyright © 2018 SchoolSpeak. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DashboardMemberVM.h"

@interface DashBoardMemberAssociationsVM : UIView

@property (nonatomic, strong) DashboardMemberVM *currentSelectedMember;
@property (nonatomic, strong) NSMutableArray <DashboardMemberVM*> *associatedMembers;

@end
