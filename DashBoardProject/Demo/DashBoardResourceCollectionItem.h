//
//  DashBoardResourcesCollectionItem.h
//  DashBoardProject
//
//  Created by AMK on 29/03/18.
//  Copyright © 2018 SchoolSpeak. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DashBoardResourceCollectionItem : NSObject

@property (nonatomic, strong) NSString *resourceDisplayName;
@property (nonatomic, strong) NSString *resourceImage;
@property (nonatomic, strong) NSString *notificationsCount;
@end
