//
//  ViewController.m
//  DashBoardProject
//
//  Created by A M K on 22/03/18.
//  Copyright © 2018 SchoolSpeak. All rights reserved.
//

#import "SSDashBoardViewController.h"
#import "DashboardResourceCollectionViewCell.h"
#import "DashboardUserProfileCollectionViewCell.h"
#import "DashboardUserProfile.h"
#import "DashBoardViewInteractor.h"

static const CGFloat cornerRadiusScaleFactorWRTLabel = 0.500527;
static const CGFloat notificationLabelScaleFactorToCell = 0.178571;
static const CGFloat profileImageViewScaleFactorToCell = 0.6;


@interface SSDashBoardViewController ()<UICollectionViewDelegateFlowLayout, UICollectionViewDataSource>

@property (nonatomic, strong) DashBoardViewInteractor *interactor;

@property (strong, nonatomic) IBOutlet UICollectionView *dashBoardResourceCollectionView;
@property (strong, nonatomic) IBOutlet UICollectionView *dashBoardUserProfileCollectionView;
@property (strong, nonatomic) IBOutlet DashboardUserProfile *currentUserView;

@property (nonatomic, strong) DashBoardMemberAssociationsVM *currentAssociationsVM;
@property (nonatomic, strong) NSArray *resourcesDisplayListForCurrentMember;


@property (nonatomic, assign) CGSize screenSize;

@end

@implementation SSDashBoardViewController

#pragma mark - Getters

- (DashBoardViewInteractor *)interactor{
    
    if(!_interactor){
        
        _interactor = [DashBoardViewInteractor new];
    }return _interactor;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.screenSize = [UIScreen mainScreen].bounds.size;
    [self registerCellNibsForCollectionViews];
    [self reloadData];
}

- (void)registerCellNibsForCollectionViews{
    
    [self.dashBoardResourceCollectionView registerNib:[UINib nibWithNibName:@"DashboardResourceCollectionViewCell" bundle:nil]
                           forCellWithReuseIdentifier:@"resourceCell"];
    
    [self.dashBoardUserProfileCollectionView registerNib:[UINib nibWithNibName:@"DashboardUserProfileCollectionViewCell" bundle:nil]
                              forCellWithReuseIdentifier:@"memberCell"];
    
    self.dashBoardResourceCollectionView.backgroundColor = [UIColor groupTableViewBackgroundColor];
    self.dashBoardUserProfileCollectionView.backgroundColor = [UIColor groupTableViewBackgroundColor];
}


- (void)reloadData{
    
    self.currentAssociationsVM = [self.interactor currentMemberAssociations];
    self.currentUserView.viewModel = self.currentAssociationsVM.currentSelectedMember;
    self.resourcesDisplayListForCurrentMember = [self.interactor resourcesDisplayListForCurrentSelectedMember];
    
    [self.currentUserView reloadData];
    [self.dashBoardResourceCollectionView reloadData];
    [self.dashBoardUserProfileCollectionView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - collection view data source

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    if(collectionView == self.dashBoardUserProfileCollectionView){
        
        //please don't write 2 instead of "2.0" , beacause fraction will be lost if we use 2
        return ceil(self.currentAssociationsVM.associatedMembers.count/2.0);
    }else{
    
        return ceil(self.resourcesDisplayListForCurrentMember.count/3.0);
    }
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    if(collectionView == self.dashBoardUserProfileCollectionView){
        
        NSInteger maxItemsOccuranceUptoSection = (section + 1) * 2;
        if(self.currentAssociationsVM.associatedMembers.count < maxItemsOccuranceUptoSection){
            
            return 1;
        }else{
            
            return 2;
        }
    }else{
        
        NSInteger maxItemsOccuranceUptoSection = (section + 1) * 3;
        if(self.resourcesDisplayListForCurrentMember.count < maxItemsOccuranceUptoSection){
            
            NSInteger freeSpacesInSection = maxItemsOccuranceUptoSection - self.resourcesDisplayListForCurrentMember.count;
            return (3 - freeSpacesInSection);
            
        }else{
            
            return 3;
        }
    }
}

- (__kindof UICollectionViewCell*)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    
    if(collectionView == self.dashBoardUserProfileCollectionView){
        
        DashboardUserProfileCollectionViewCell *cell = [self.dashBoardUserProfileCollectionView
                                                             dequeueReusableCellWithReuseIdentifier:@"memberCell" forIndexPath:indexPath];
        cell.viewModel = [self.currentAssociationsVM.associatedMembers objectAtIndex:
                                                                       indexPath.section*2+indexPath.row];
        [cell reloadData];
        return cell;
    }else{
        
        DashboardResourceCollectionViewCell *cell = [self.dashBoardResourceCollectionView
                                                             dequeueReusableCellWithReuseIdentifier:@"resourceCell" forIndexPath:indexPath];
        cell.viewModel = [self.resourcesDisplayListForCurrentMember objectAtIndex:
                                                                       indexPath.section*3+indexPath.row];
        [cell reloadData];
        return cell;
    }
}

#pragma mark - Collection view delegate

- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell
                                                            forItemAtIndexPath:(NSIndexPath *)indexPath{
    
    if(collectionView == self.dashBoardResourceCollectionView){
        
        DashboardResourceCollectionViewCell *dashboardCell = (DashboardResourceCollectionViewCell*)cell;
        dashboardCell.notificationCountLabel.layer.cornerRadius = dashboardCell.bounds.size.width * notificationLabelScaleFactorToCell *
                                                                  cornerRadiusScaleFactorWRTLabel;
    }else{
        
         DashboardUserProfileCollectionViewCell *dashboardUserCell = (DashboardUserProfileCollectionViewCell *)cell;
        dashboardUserCell.profileImageView.layer.cornerRadius = dashboardUserCell.bounds.size.height *
                                                                profileImageViewScaleFactorToCell *
                                                                cornerRadiusScaleFactorWRTLabel;
    }
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    if(collectionView == self.dashBoardUserProfileCollectionView){
        
        DashboardMemberVM *selectedMember = [self.currentAssociationsVM.associatedMembers
                                             objectAtIndex:indexPath.section*2+indexPath.row];
        [self.interactor updateMemberAssociation:selectedMember];
        [self reloadData];
    }else{
    
        //code here for operations to be performed after click on resource name
    }
    
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout
                                                                sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    if(collectionView == self.dashBoardUserProfileCollectionView){
        
        CGFloat height = 0;
        CGFloat width = 0;
        
        height = (self.dashBoardUserProfileCollectionView.bounds.size.height/2)-1;
        width = self.currentAssociationsVM.associatedMembers.count<3?self.dashBoardUserProfileCollectionView.bounds.size.width-2:
                                                                self.dashBoardUserProfileCollectionView.bounds.size.width*0.85;
        
        return CGSizeMake(width, height);
    }else{
        
        return CGSizeMake((self.dashBoardResourceCollectionView.bounds.size.width-3)/3,(self.dashBoardResourceCollectionView.bounds.size.width-3)/3);
    }
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionView *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section{
    
    return 0; // This is the minimum inter item spacing, can be more
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{

    if(collectionView == self.dashBoardResourceCollectionView){

        return UIEdgeInsetsMake(0.5, 0, 0.5, 0);
    }else{
        
        return UIEdgeInsetsZero;
    }
}

@end
