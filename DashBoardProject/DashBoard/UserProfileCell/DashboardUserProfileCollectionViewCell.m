//
//  DashboardUserProfileCollectionViewCell.m
//  DashBoardProject
//
//  Created by AMK on 26/03/18.
//  Copyright © 2018 SchoolSpeak. All rights reserved.
//

#import "DashboardUserProfileCollectionViewCell.h"

@implementation DashboardUserProfileCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)reloadData{
    
    self.profileNameLabel.text = self.viewModel.displayName;
    self.profileImageView.image = [UIImage imageNamed:self.viewModel.imageName];
    
}
@end
