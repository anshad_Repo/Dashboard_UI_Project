//
//  DashboardUserProfileCollectionViewCell.h
//  DashBoardProject
//
//  Created by AMK on 26/03/18.
//  Copyright © 2018 SchoolSpeak. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DashboardMemberVM.h"

@interface DashboardUserProfileCollectionViewCell : UICollectionViewCell

@property (strong, nonatomic) IBOutlet UIImageView *profileImageView;
@property (strong, nonatomic) IBOutlet UILabel *profileNameLabel;
@property (strong, nonatomic) IBOutlet UIView *verticalSeperatorView;

@property (nonatomic, strong) DashboardMemberVM *viewModel;

- (void)reloadData;

@end
