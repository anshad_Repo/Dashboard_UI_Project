
//  DashboardUserProfileView.m
//  DashBoardProject
//
//  Created by AMK on 27/03/18.
//  Copyright © 2018 SchoolSpeak. All rights reserved.
//

#import "DashboardUserProfile.h"

@implementation DashboardUserProfile

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void)awakeFromNib{
    
    [super awakeFromNib];
    [self setUpView];
}

- (void)setUpView{
    
    NSBundle* bundle = [NSBundle bundleForClass:[self class]];
    UINib* nib = [UINib nibWithNibName:@"DashboardUserProfileView" bundle:bundle];
    UIView* view = [nib instantiateWithOwner:self options:nil][0];
    view.frame = self.bounds;
    [self addSubview:view];
    
    self.userImageView.layer.cornerRadius = self.bounds.size.height*0.5* 0.5;
    self.translatesAutoresizingMaskIntoConstraints = NO;
    self.layer.cornerRadius = 4.0;
}

- (void)reloadData{
    
    self.profileNameLabel.text = self.viewModel.displayName;
    self.userImageView.image = [UIImage imageNamed:self.viewModel.imageName];
}

@end
