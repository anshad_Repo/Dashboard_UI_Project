//
//  DashboardUserProfileView.h
//  DashBoardProject
//
//  Created by AMK on 27/03/18.
//  Copyright © 2018 SchoolSpeak. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DashboardMemberVM.h"

@interface DashboardUserProfile : UIView

@property (strong, nonatomic) IBOutlet UILabel *profileNameLabel;
@property (strong, nonatomic) IBOutlet UIImageView *userImageView;

@property (nonatomic, strong) DashboardMemberVM *viewModel;

- (void)reloadData;

@end
