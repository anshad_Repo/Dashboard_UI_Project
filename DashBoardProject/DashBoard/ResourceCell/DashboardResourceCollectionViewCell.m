//
//  DashboardResourceCollectionViewCell.m
//  DashBoardProject
//
//  Created by A M K on 22/03/18.
//  Copyright © 2018 SchoolSpeak. All rights reserved.
//

#import "DashboardResourceCollectionViewCell.h"


@implementation DashboardResourceCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}


- (void)reloadData{
    
    self.resourceNameLabel.text = self.viewModel.resourceDisplayName;
    self.iconView.image = [UIImage imageNamed:self.viewModel.resourceImage];
    if(self.viewModel.notificationsCount.length){
        
        self.notificationCountLabel.text = self.viewModel.notificationsCount;
        self.notificationCountLabel.hidden = NO;
    }else{
        
        self.notificationCountLabel.hidden = YES;
    }
}

@end
