//
//  DashboardResourceCollectionViewCell.h
//  DashBoardProject
//
//  Created by A M K on 22/03/18.
//  Copyright © 2018 SchoolSpeak. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DashBoardResourceCollectionItem.h"

@interface DashboardResourceCollectionViewCell : UICollectionViewCell

@property (strong, nonatomic) IBOutlet UILabel *resourceNameLabel;
@property (strong, nonatomic) IBOutlet UIImageView *iconView;
@property (strong, nonatomic) IBOutlet UILabel *notificationCountLabel;

@property (nonatomic, strong) DashBoardResourceCollectionItem *viewModel;

- (void)reloadData;

@end
